<?php

namespace Drupal\youtube_duration\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\duration_field\Service\DurationServiceInterface;
use Drupal\media\Entity\MediaType;
use Drupal\media\OEmbed\ResourceException;
use Drupal\media\OEmbed\UrlResolverInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

class YoutubeDurationGetterService {

  use StringTranslationTrait;

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var \Drupal\duration_field\Service\DurationServiceInterface
   */
  protected $durationFieldService;

  /**
   * @var \Drupal\media\OEmbed\UrlResolverInterface
   */
  protected $urlResolver;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\media\Entity\MediaType
   */
  protected $mediaType;

  /**
   * @var array
   *   media type third party settings
   */
  protected $settings;

  /**
   * @var EntityInterface
   */
  protected $entity;

  /**
   * @var \Drupal\media\OEmbed\Provider
   */
  protected $provider;

  /**
   * @var bool | string
   *   false or ISO 8601 duration string
   */
  protected $youtubeDuration = FALSE;

  /**
   * @var array
   */
  protected $sourceConfiguration;

  /**
   * @var string
   */
  protected $oEmbedUrl;

  /**
   * @var string
   */
  protected $oEmbedOriginalUrl;

  /**
   * @var string
   */
  protected $youtubeVideoId;

  /**
   * @var string
   */
  protected $youtubeApiKey;

  /**
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  protected $durationField;

  /**
   * YoutubeDurationGetterService constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   * @param \Drupal\duration_field\Service\DurationServiceInterface $duration_service
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(ClientInterface $http_client, DurationServiceInterface $duration_service, UrlResolverInterface $url_resolver, MessengerInterface $messenger) {
    $this->httpClient = $http_client;
    $this->durationFieldService = $duration_service;
    $this->urlResolver = $url_resolver;
    $this->messenger = $messenger;
  }

  /**
   * init the duration getter with the variables necessary to get the duration
   *
   * @param \Drupal\media\Entity\MediaType $media_type
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public function initDurationGetter(MediaType $media_type, EntityInterface $entity) {
    $this->mediaType = $media_type;
    $this->settings = $media_type->getThirdPartySettings('youtube_duration');
    if (!empty($this->settings['duration_enabled'])) {
      $this->durationField = $entity->{$this->settings['duration_field']};
      $this->youtubeApiKey = $this->settings['duration_apikey'];
      $this->sourceConfiguration = $media_type->getSource()->getConfiguration();
      $this->entity = $entity;
      $this->oEmbedUrl = $entity->{$this->sourceConfiguration['source_field']}->value;
      try {
        $this->provider = $this->urlResolver->getProviderByUrl($this->oEmbedUrl);
      }
      catch (ResourceException $e) {
        return;
      }
      if ($this->provider->getName() == 'YouTube') {
        $this->oEmbedOriginalUrl = $entity->original->{$this->sourceConfiguration['source_field']}->value ?? FALSE;
        if ($this->detectChangeOfSourceValue() || $this->detectEmptyDuration()) {
          $this->setVideoId();
          $this->setYoutubeDuration();
          $this->updateEntity($entity);
        }
      }
    }
  }

  /**
   * update the entity with the duration
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  protected function updateEntity(EntityInterface $entity) {
    if ($this->youtubeDuration) {
      $entity->{$this->settings['duration_field']}->duration = $this->youtubeDuration;
      $entity->{$this->settings['duration_field']}->seconds = $this->durationFieldService
        ->getSecondsFromDurationString($this->youtubeDuration);
    }
  }

  /**
   * should the entity be updated
   *
   * has there been a change or is it a new entity.
   *
   * @return bool
   */
  protected function detectChangeOfSourceValue() {
    return empty($this->oEmbedOriginalUrl) || $this->oEmbedUrl != $this->oEmbedOriginalUrl;
  }

  /**
   * is the duration empty
   *
   * @return bool
   */
  protected function detectEmptyDuration() {
    return !empty($this->durationField) && $this->durationField->isEmpty();
  }

  /**
   * getter for youtube duration ISO 8601 string
   *
   * @return bool | string
   */
  public function getYoutubeDuration() {
    return $this->youtubeDuration;
  }

  /**
   * setting the youtube duration by contacting youtube API
   */
  protected function setYoutubeDuration() {
    if (!empty($this->youtubeVideoId) && !empty($this->youtubeApiKey)) {
      $url = 'https://www.googleapis.com/youtube/v3/videos?id=' . $this->youtubeVideoId . '&part=contentDetails&key=' . $this->youtubeApiKey;
      try {
        $response = $this->httpClient->get($url);
      } catch (ClientException $e) {
        $this->messenger->addError($this->t('Error connecting Youtube API: Error code @error, Error message: @message',
          [
            '@error' => $e->getCode(),
            '@message' => $this->getErrorMessage($e)
          ]
        ));
        return;
      }
      $body = (string) $response->getBody();
      $content = Json::decode($body);
      $this->youtubeDuration = isset($content['items'][0]['contentDetails']['duration']) ? $content['items'][0]['contentDetails']['duration'] : FALSE;
    }
  }

  /**
   * get error message from json body contents
   */
  protected function getErrorMessage(ClientException $exception) {
    $json = json_decode($exception->getResponse()->getBody()->getContents());
    return $json->error->message ?? $this->t('No message');
  }

  /**
   * setting the Youtube video ID extracted from OEmbed URL
   */
  protected function setVideoId() {
    parse_str(parse_url($this->oEmbedUrl, PHP_URL_QUERY) ?? '', $youtubeUrl);
    // Urls like https://www.youtube.com/watch?v=hTWKbfoikeg
    if (isset($youtubeUrl['v'])) {
      $this->youtubeVideoId = $youtubeUrl['v'];
    }
    else {
      // Urls like https://youtu.be/hTWKbfoikeg?feature=shared
      $path = trim(parse_url($this->oEmbedUrl, PHP_URL_PATH), '/');
      // supports shorts too
      $path = str_replace('shorts/', '', $path);
      if ($path && strpos($path, '/') === FALSE) {
        $this->youtubeVideoId = $path;
      }
    }
  }

  /**
   * getter for oEmbed URL
   *
   * @return string
   */
  protected function getOembedUrl() {
    return $this->oEmbedUrl;
  }

}
